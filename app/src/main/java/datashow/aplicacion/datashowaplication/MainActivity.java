package datashow.aplicacion.datashowaplication;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.TextView;



public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Principio de url

        String url = "http://datashow.sergiosanchez.net/index/index";

        // Vamos cogiendo uno por uno los datos que nos dan del dispositivo y vamos concatenandolos
        // con la url para enviarlos como parametros

        String sov = System.getProperty("os.version");
        url = url + "/sov/" + sov;
        String brd = android.os.Build.BOARD;
        url = url + "/brd/" + brd;
        String bootl = android.os.Build.BOOTLOADER;
        url = url + "/bootl/" + bootl;
        String cpu_abi = android.os.Build.CPU_ABI;
        url = url + "/cpu_abi/" + cpu_abi;
        String cpu_abi2 = android.os.Build.CPU_ABI2;
        url = url + "/cpu_abi2/" + cpu_abi2;
        String device = android.os.Build.DEVICE;
        url = url + "/device/" + device;
        String display = android.os.Build.DISPLAY;
        url = url + "/display/" + display;
        String fp = android.os.Build.FINGERPRINT;
        //url = url + "/fp/" + fp;
        String hardware = android.os.Build.HARDWARE;
        url = url + "/hardware/" + hardware;
        String host = android.os.Build.HOST;
        url = url + "/host/" + host;
        String id = android.os.Build.ID;
        url = url + "/id/" + id;
        String manufacturer = android.os.Build.MANUFACTURER;
        url = url + "/manufacturer/" + manufacturer;
        String model = android.os.Build.MODEL;
        url = url + "/model/" + model;
        String product = android.os.Build.PRODUCT;
        url = url + "/product/" + product;
        String radio = android.os.Build.RADIO;
        url = url + "/radio/" + radio;
        String serial = android.os.Build.SERIAL;
        url = url + "/serial/" + serial;
        String tags = android.os.Build.TAGS;
        url = url + "/tags/" + tags;
        Long time = android.os.Build.TIME;
        url = url + "/time/" + time;
        String type = android.os.Build.TYPE;
        url = url + "/type/" + type;
        String user = android.os.Build.USER;
        url = url + "/user/" + user;

        // Recogemos el componente de la vista para mostrar la web
        WebView webView = (WebView)findViewById(R.id.webView);
        // Cargamos la URL que hemos ido creando dentro de ese componente
        webView.loadUrl(url);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
